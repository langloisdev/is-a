jest.mock(`./src/arguments.js`, () => ({ isArguments: `fake-isArguments` }));
jest.mock(`./src/array.js`, () => ({ isArray: `fake-isArray` }));
jest.mock(`./src/boolean.js`, () => ({ isBoolean: `fake-isBoolean` }));
jest.mock(`./src/date.js`, () => ({ isDate: `fake-isDate` }));
jest.mock(`./src/email.js`, () => ({ isEmail: `fake-isEmail` }));
jest.mock(`./src/error.js`, () => ({ isError: `fake-isError` }));
jest.mock(`./src/function.js`, () => ({ isFunction: `fake-isFunction` }));
jest.mock(`./src/map.js`, () => ({ isMap: `fake-isMap` }));
jest.mock(`./src/NaN.js`, () => ({ isNaN: `fake-isNaN` }));
jest.mock(`./src/null.js`, () => ({ isNull: `fake-isNull` }));
jest.mock(`./src/number.js`, () => ({ isNumber: `fake-isNumber` }));
jest.mock(`./src/numerical.js`, () => ({ isNumerical: `fake-isNumerical` }));
jest.mock(`./src/object.js`, () => ({ isObject: `fake-isObject` }));
jest.mock(`./src/regExp.js`, () => ({ isRegExp: `fake-isRegExp` }));
jest.mock(`./src/set.js`, () => ({ isSet: `fake-isSet` }));
jest.mock(`./src/string.js`, () => ({ isString: `fake-isString` }));
jest.mock(`./src/symbol.js`, () => ({ isSymbol: `fake-isSymbol` }));
jest.mock(`./src/undefined.js`, () => ({ isUndefined: `fake-isUndefined` }));
jest.mock(`./src/uri.js`, () => ({ isUri: `fake-isUri` }));
jest.mock(`./src/uuid.js`, () => ({ isUuid: `fake-isUuid` }));
jest.mock(`./src/weakMap.js`, () => ({ isWeakMap: `fake-isWeakMap` }));
jest.mock(`./src/weakSet.js`, () => ({ isWeakSet: `fake-isWeakSet` }));

import { isA } from './index';

describe(`isA`, () => {
	test(`exists`, () => {
		expect(isA).toBeDefined();
	});

	test(`is an Object`, () => {
		expect(isA).toBeInstanceOf(Object);
	});

	describe.each(Object.entries({
		arguments: `fake-isArguments`,
		array: `fake-isArray`,
		boolean: `fake-isBoolean`,
		date: `fake-isDate`,
		email: `fake-isEmail`,
		error: `fake-isError`,
		function: `fake-isFunction`,
		map: `fake-isMap`,
		NaN: `fake-isNaN`,
		null: `fake-isNull`,
		number: `fake-isNumber`,
		numerical: `fake-isNumerical`,
		object: `fake-isObject`,
		regExp: `fake-isRegExp`,
		set: `fake-isSet`,
		string: `fake-isString`,
		symbol: `fake-isSymbol`,
		undefined: `fake-isUndefined`,
		uri: `fake-isUri`,
		uuid: `fake-isUuid`,
		weakMap: `fake-isWeakMap`,
		weakSet: `fake-isWeakSet`
	}))(`%s function`, (key, value) => {
		it(`exists`, () => {
			expect(isA[key]).toBeDefined();
		});

		it(`is the is the ${key} function`, () => {
			expect(isA[key]).toStrictEqual(value);
		});
	});
});
