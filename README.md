# Javascript Type Verification Library
A small JavaScript library to help with JavaScript's data typing.

## Project setup

```
npm i @langlois/is-a
```
### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

## Usage
```js
import { isA } from '@langlois/is-a'

isA.array([]) // true

isA.array('[]') // false
```

## API
### Arguments
Returns `true` if passed a function's arguments list.

```js
function () {
	isA.arguments(arguments) // true

	isA.arguments(arguments[0]) // false
}
```

### Array
Returns `true` if passed an array.

```js
isA.array([]) // true
```

### Boolean
Returns `true` if passed a boolean.

```js
isA.boolean(true) // true

isA.boolean(false) // true
```

### Date
Returns `true` if passed a date.

```js
const today = new Date()

isA.date(today) // true
```

### Email
Returns `true` if the string passed is a valid email address.

The regex for determining if an email is valid is taken from the [HTML spec](https://html.spec.whatwg.org/multipage/input.html#valid-e-mail-address).

```js
const email = 'test@test.com'

isA.email(email) // true
```

### Error
Returns `true` if passed an error.

```js
const error = new Error()

isA.error(error) // true
```

### Function
Returns `true` if passed a function.

```js
const myFunction = () => undefined

isA.function(myFunction) // true
```

### Map
Returns `true` if passed a map.

```js
const myMap = new Map()

isA.map(myMap) // true
```

### NaN
Returns `true` if passed `NaN`.

```js
isA.NaN(NaN) // true
```

### Null
Returns `true` if passed `null`.

```js
isA.null(null) // true
```

### Number
Returns `true` if passed a number, excluding `NaN`.

```js
isA.number(123) // true

isA.number(NaN) // false
```

### Numerical
Returns `true` if passed a number or a string representing a number, excluding `NaN`.

```js
isA.numerical(23) // true

isA.numerical('23') // true

isA.numerical('-23') // true

isA.numerical(NaN) // false
```

### Object
Returns `true` if passed an object.

```js
isA.object({}) // true
```

Everything in JavaScript is an Object and that means that many data types return `typeof x === 'object'` as `true` as well as `x instanceof Object` as `true`. However, it isn't really useful to treat certain things as if they were objects when developing so this function will return `false` for the following data types:
- dates
- errors
- maps
- regExps
- sets
- weakMaps
- weakSets

```js
isA.object(new Date()) // false
isA.object(new Error()) // false
isA.object(new Map()) // false
isA.object(/\d/) // false
isA.object(new Set()) // false
isA.object(new WeakMap()) // false
isA.object(new WeakSet()) // false
```

### RegExp
Returns `true` if passed a regExp.

```js
const myRegExp = /\d/

isA.regExp(myRegExp) // true
```

### Set
Returns `true` if passed a set.

```js
const mySet = new Set()

isA.set(mySet) //  true
```

### String
Returns `true` if passed a string.

```js
isA.string('hello, world') //  true
```

### Symbol
Returns `true` if passed a symbol.

```js
const mySymbol = Symbol()

isA.symbol(mySymbol) //  true
```

### Undefined
Returns `true` if passed undefined.

```js
isA.undefined(undefined) //  true
```

### URI
Returns `true` if passed a URL instance or a string that can become a valid URL instance.

```js
const myUrl = new URL('https://langlois.dev')

isA.uri(myUrl) //  true
isA.uri('https://langlois.dev') // true
```

### WeakMap
Returns `true` if passed a weakMap.

```js
const myWeakMap = new WeakMap()

isA.weakMap(myWeakMap) //  true
```

### WeakSet
Returns `true` if passed a weakSet.

```js
const myWeakSet = new WeakSet()

isA.weakSet(myWeakSet) //  true
```
