export const testArray = [];

export const testDate = new Date();

export const testError = new Error();

export function testFunction () { return undefined; };

export const testArrowFunction = () => undefined;

export const testMap = new Map();

export const testNumber = 123;
export const testNegativeNumber = -123;

export const testDecimalNumber = 12.3;
export const testNegativeDecimalNumber = -12.3;

export const testZero = 0;
export const testNegativeZero = -0;

export const testInfinity = Infinity;

export const testNaN = NaN;

export const testNull = null;

export const testUndefined = undefined;

export const testObject = {};

export const testRegExp = /\d/;

export const testSet = new Set();

export const testStringEmpty = ``;

export const testStringText = `Hello, world.`;

export const testStringNumber = `123`;
export const testStringNegativeNumber = `-123`;

export const testStringDecimalNumber = `12.3`;
export const testStringNegativeDecimalNumber = `-12.3`;

export const testStringZero = `0`;
export const testStringNegativeZero = `-0`;

export const testTrue = true;
export const testFalse = false;

export const testSymbol = Symbol(`foo`);

export const testWeakMap = new WeakMap();

export const testWeakSet = new WeakSet();

export const testItems = {
	testArray,
	testArrowFunction,
	testDate,
	testDecimalNumber,
	testError,
	testFalse,
	testFunction,
	testInfinity,
	testMap,
	testNaN,
	testNegativeDecimalNumber,
	testNegativeNumber,
	testNegativeZero,
	testNull,
	testNumber,
	testObject,
	testRegExp,
	testSet,
	testStringDecimalNumber,
	testStringEmpty,
	testStringNegativeDecimalNumber,
	testStringNegativeNumber,
	testStringNegativeZero,
	testStringNumber,
	testStringText,
	testStringZero,
	testSymbol,
	testTrue,
	testUndefined,
	testWeakMap,
	testWeakSet,
	testZero
};
