import { isArguments } from './src/arguments.js';
import { isArray } from './src/array.js';
import { isBoolean } from './src/boolean.js';
import { isDate } from './src/date.js';
import { isEmail } from './src/email.js';
import { isError } from './src/error.js';
import { isFunction } from './src/function.js';
import { isMap } from './src/map.js';
import { isNaN } from './src/NaN.js';
import { isNumber } from './src/number.js';
import { isNumerical } from './src/numerical.js';
import { isNull } from './src/null.js';
import { isObject } from './src/object.js';
import { isRegExp } from './src/regExp.js';
import { isSet } from './src/set.js';
import { isString } from './src/string.js';
import { isSymbol } from './src/symbol.js';
import { isUndefined } from './src/undefined.js';
import { isUri } from './src/uri.js';
import { isUuid } from './src/uuid.js';
import { isWeakMap } from './src/weakMap.js';
import { isWeakSet } from './src/weakSet.js';

export const isA = {
	arguments: isArguments,
	array: isArray,
	boolean: isBoolean,
	date: isDate,
	email: isEmail,
	error: isError,
	function: isFunction,
	map: isMap,
	NaN: isNaN,
	null: isNull,
	number: isNumber,
	numerical: isNumerical,
	object: isObject,
	regExp: isRegExp,
	set: isSet,
	string: isString,
	symbol: isSymbol,
	undefined: isUndefined,
	uri: isUri,
	uuid: isUuid,
	weakMap: isWeakMap,
	weakSet: isWeakSet
};
