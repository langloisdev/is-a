const path = require(`path`);

module.exports = [
	`source-map`
].map(devtool => ({
	devtool,
	entry: `./index.js`,
	mode: `production`,
	output: {
		filename: `index.js`,
		library: `isA`,
		libraryTarget: `umd`,
		path: path.resolve(__dirname, `lib`)
	},
	target: `node`
}));
