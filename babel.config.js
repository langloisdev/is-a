module.exports = api => {
	api.cache(true);

	return {
		plugins: [
			`@babel/plugin-syntax-import-meta`,
			`@babel/plugin-transform-runtime`
		],
		presets: [
			[`@babel/preset-env`, {
				targets: {
					node: true
				}
			}]
		]
	};
};
