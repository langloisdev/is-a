import { isString } from './string.js';

/**
 * Evalutes if the item passed is an email.
 * Email RegEx [from]{@link https://html.spec.whatwg.org/multipage/input.html#valid-e-mail-address} doesn't validate unicode characters. We instead have a very liberal check where we look for an "@" symbol along with anything before and after the symbol.
 *
 * @name isEmail
 * @function
 * @param {*} item - The data who's type is being evaluted.
 *
 * @example
 *
 * isEmail(`francis@langlois.dev`); // true
 */
export function isEmail (item) {
	if (isString(item)) {
		const emailRegExp = new RegExp(
			/^[^@]+@[^@]+$/
		);

		return emailRegExp.test(item);
	}

	return false;
}
