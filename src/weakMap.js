/**
 * Evalutes if the item passed is a weak map.
 *
 * @name isWeakMap
 * @function
 * @param {*} item - The data who's type is being evaluted.
 *
 * @example
 *
 * isWeakMap(new WeakMap()); // true
 */
export function isWeakMap (item) {
	return Object.prototype.toString.call(item) === `[object WeakMap]`;
}
