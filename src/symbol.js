/**
 * Evalutes if the item passed is a symbol.
 *
 * @name isSymbol
 * @function
 * @param {*} item - The data who's type is being evaluted.
 *
 * @example
 *
 * Symbol(`newSymbol`); // true
 */
export function isSymbol (item) {
	return Object.prototype.toString.call(item) === `[object Symbol]`;
}
