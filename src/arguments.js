/**
 * Evalutes if the item passed is a list of a function's arguments.
 *
 * @name isArguments
 * @function
 * @param {*} item - The data who's type is being evaluted.
 *
 * @example
 *
 * function withArguments() {
 *     isArguments(arguments); // true
 * }
 */
export function isArguments (item) {
	return Object.prototype.toString.call(item) === `[object Arguments]`;
}
