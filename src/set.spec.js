import { isSet } from './set.js';
import { testItems } from '../tests/testItems.js';

delete testItems.testSet;

describe(`is set function`, () => {
	test(`exists`, () => {
		expect(isSet).toBeDefined();
	});

	test(`is a Function`, () => {
		expect(isSet).toBeInstanceOf(Function);
	});

	test(`returns true if the value passed is a set`, () => {
		expect(isSet(new Set())).toStrictEqual(true);
	});

	test.each(Object.entries(testItems))(`returns false if the value passed is %s`, (key, value) => {
		expect(isSet(value)).toStrictEqual(false);
	});
});
