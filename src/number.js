import { isNaN } from './NaN.js';

/**
 * Evalutes if the item passed is a number.
 *
 * @name isNumber
 * @function
 * @param {*} item - The data who's type is being evaluted.
 *
 * @example
 *
 * isNumber(-1.2); // true
 * isNumber(-1); // true
 * isNumber(0); // true
 * isNumber(1); // true
 * isNumber(1.2); // true
 *
 * isNumber(Infinity); // true
 *
 * isNumber(NaN); // false
 */
export function isNumber (item) {
	return Object.prototype.toString.call(item) === `[object Number]` && !isNaN(item);
}
