import { isFunction } from './function.js';
import { testItems } from '../tests/testItems.js';

delete testItems.testFunction;
delete testItems.testArrowFunction;

describe(`is function function`, () => {
	test(`exists`, () => {
		expect(isFunction).toBeDefined();
	});

	test(`is a Function`, () => {
		expect(isFunction).toBeInstanceOf(Function);
	});

	test.each([
		() => undefined,
		function foo () {}
	])(`returns true if the value passed is a function`, func => {
		expect(isFunction(func)).toStrictEqual(true);
	});

	test.each(Object.entries(testItems))(`returns false if the value passed is %s`, (key, value) => {
		expect(isFunction(value)).toStrictEqual(false);
	});
});
