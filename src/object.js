import { isArray } from './array.js';
import { isDate } from './date.js';
import { isError } from './error.js';
import { isMap } from './map.js';
import { isRegExp } from './regExp.js';
import { isSet } from './set.js';
import { isWeakMap } from './weakMap.js';
import { isWeakSet } from './weakSet.js';

/**
 * Evalutes if the item passed is an object.
 *
 * @name isObject
 * @function
 * @param {*} item - The data who's type is being evaluted.
 *
 * @example
 *
 * isObject({}); // true
 */
export function isObject (item) {
	const otherInstance = [
		isArray(item),
		isDate(item),
		isError(item),
		isMap(item),
		isRegExp(item),
		isSet(item),
		isWeakMap(item),
		isWeakSet(item)
	];

	return typeof item === `object` && !!item && !otherInstance.includes(true);
}
