import { isBoolean } from './boolean.js';
import { testItems } from '../tests/testItems.js';

delete testItems.testTrue;
delete testItems.testFalse;

describe(`is boolean function`, () => {
	test(`exists`, () => {
		expect(isBoolean).toBeDefined();
	});

	test(`is a Function`, () => {
		expect(isBoolean).toBeInstanceOf(Function);
	});

	test.each([true, false])(`returns true if the value passed is boolean %s`, boolean => {
		expect(isBoolean(boolean)).toStrictEqual(true);
	});

	test.each(Object.entries(testItems))(`returns false if the value passed is %s`, (key, value) => {
		expect(isBoolean(value)).toStrictEqual(false);
	});
});
