import { isString } from './string.js';
import { testItems } from '../tests/testItems.js';

delete testItems.testStringDecimalNumber;
delete testItems.testStringEmpty;
delete testItems.testStringNegativeDecimalNumber;
delete testItems.testStringNegativeNumber;
delete testItems.testStringNegativeZero;
delete testItems.testStringNumber;
delete testItems.testStringText;
delete testItems.testStringZero;

describe(`is string function`, () => {
	test(`exists`, () => {
		expect(isString).toBeDefined();
	});

	test(`is a Function`, () => {
		expect(isString).toBeInstanceOf(Function);
	});

	test.each([
		`1.2`,
		`-1.2`,
		`-1`,
		`-0`,
		``,
		` `,
		`abc`,
		`0`,
		`1`,
		`undefined`,
		`null`,
		`NaN`,
		`Infinity`,
		{ a: true }.toString()
	])(`returns true if the value passed is string "%s"`, string => {
		expect(isString(string)).toStrictEqual(true);
	});

	test.each(Object.entries(testItems))(`returns false if the value passed is %s`, (key, value) => {
		expect(isString(value)).toStrictEqual(false);
	});
});
