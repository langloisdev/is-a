import { isArguments } from './arguments.js';
import { testItems } from '../tests/testItems.js';

describe(`is arguments function`, () => {
	test(`exists`, () => {
		expect(isArguments).toBeDefined();
	});

	test(`is a Function`, () => {
		expect(isArguments).toBeInstanceOf(Function);
	});

	test(`returns true if the value passed is an argument`, () => {
		function testArguments () {
			return isArguments(arguments);
		}

		expect(testArguments()).toStrictEqual(true);
	});

	test.each(Object.entries(testItems))(`returns false if the value passed is not an argument`, (key, value) => {
		expect(isArguments(value)).toStrictEqual(false);
	});
});
