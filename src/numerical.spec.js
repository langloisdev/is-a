import { isNumerical } from './numerical.js';
import { testItems } from '../tests/testItems.js';

delete testItems.testDecimalNumber;
delete testItems.testInfinity;
delete testItems.testNegativeDecimalNumber;
delete testItems.testNegativeNumber;
delete testItems.testNegativeZero;
delete testItems.testNumber;
delete testItems.testStringDecimalNumber;
delete testItems.testStringNegativeDecimalNumber;
delete testItems.testStringNegativeNumber;
delete testItems.testStringNegativeZero;
delete testItems.testStringNumber;
delete testItems.testStringZero;
delete testItems.testZero;

describe(`is numerical function`, () => {
	test(`exists`, () => {
		expect(isNumerical).toBeDefined();
	});

	test(`is a Function`, () => {
		expect(isNumerical).toBeInstanceOf(Function);
	});

	test.each([
		Infinity,
		-1.23,
		-1,
		-0,
		0,
		1,
		1.23,
		`-1.23`,
		`-1`,
		`-0`,
		`0`,
		`1`,
		`1.23`
	])(`returns true if the value passed is a valid numerical value %s`, numerical => {
		expect(isNumerical(numerical)).toStrictEqual(true);
	});

	test.each(Object.entries(testItems))(`returns false if the value passed is %s`, (key, value) => {
		expect(isNumerical(value)).toStrictEqual(false);
	});
});
