import { isDate } from './date.js';
import { testItems } from '../tests/testItems.js';

delete testItems.testDate;

describe(`is date function`, () => {
	test(`exists`, () => {
		expect(isDate).toBeDefined();
	});

	test(`is a Function`, () => {
		expect(isDate).toBeInstanceOf(Function);
	});

	test(`returns true if the value passed is a date`, () => {
		expect(isDate(new Date())).toStrictEqual(true);
	});

	test.each(Object.entries(testItems))(`returns false if the value passed is %s`, (key, value) => {
		expect(isDate(value)).toStrictEqual(false);
	});
});
