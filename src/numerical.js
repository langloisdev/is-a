import { isString } from './string.js';
import { isNumber } from './number.js';

/**
 * Evalutes if the item passed is a numerical value.
 *
 * @name isNumerical
 * @function
 * @param {*} item - The data who's type is being evaluted.
 *
 * @example
 *
 * isNumerical(`1`); // true
 * isNumerical(1); // true
 */
export function isNumerical (item) {
	if ((isString(item) && item.length) || isNumber(item)) {
		const newItem = item - 0;

		return newItem === newItem;
	}
	return false;
}
