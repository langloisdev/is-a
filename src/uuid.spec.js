import { isUuid } from './uuid.js';
import { testItems } from '../tests/testItems.js';

describe(`is UUID function`, () => {
	test(`exists`, () => {
		expect(isUuid).toBeDefined();
	});

	test(`is a Function`, () => {
		expect(isUuid).toBeInstanceOf(Function);
	});

	test.each([
		`0ec51dca-f18f-418d-ac57-d9547b00d95d`,
		`560f8037-c0f8-4d74-a6ab-0107875a98ea`,
		`8a29373c-7dd6-444e-88d9-c33c8b1f87e6`,
		`3b67ff67-3622-4a29-9155-8c5460ccbc90`,
		`c53397d4-d6f0-4659-a03b-f4421a0da162`,
		`6b83a8f3-1edd-45c4-9673-657b1a265f8b`,
		`0dfaf7de-0ae6-44a3-9f63-b9287d2de37d`,
		`5e755eed-aaf8-475a-ac08-bf257807be65`,
		`26996569-a6ed-4000-923c-01e2afe46575`,
		`a84b5850-8d93-4a77-b7a9-a8786435d58e`,
		`e7c9b3a2-a1a0-4486-a221-4f03727c0d5d`
	])(`returns true if the value passed is a valid UUID "%s"`, uuid => {
		expect(isUuid(uuid)).toStrictEqual(true);
	});

	test.each(Object.entries(testItems))(`returns false if the value passed is %s`, (key, value) => {
		expect(isUuid(value)).toStrictEqual(false);
	});
});
