import { isNaN } from './NaN.js';
import { testItems } from '../tests/testItems.js';

delete testItems.testNaN;

describe(`is NaN function`, () => {
	test(`exists`, () => {
		expect(isNaN).toBeDefined();
	});

	test(`is a Function`, () => {
		expect(isNaN).toBeInstanceOf(Function);
	});

	test(`returns true if the value passed is NaN`, () => {
		expect(isNaN(NaN)).toStrictEqual(true);
	});

	let isNaNCount = 0;

	test.each(Object.entries(testItems))(`returns false if the value passed is %s`, (key, value) => {
		expect(isNaN(value)).toStrictEqual(false);
	});
});
