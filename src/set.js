/**
 * Evalutes if the item passed is a set.
 *
 * @name isSet
 * @function
 * @param {*} item - The data who's type is being evaluted.
 *
 * @example
 *
 * isSet(new Set()); // true
 */
export function isSet (item) {
	return Object.prototype.toString.call(item) === `[object Set]`;
}
