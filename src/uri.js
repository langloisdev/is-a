import { isString } from './string.js';

/**
 * Evalutes if the item passed is a URI.
 *
 * @name isUri
 * @function
 * @param {*} item - The data who's type is being evaluted.
 *
 * @example
 *
 * isUri(`https://langlois.dev`); // true
 */
export function isUri (item) {
	if (isString(item)) {
		try {
			new URL(item);

			return true;
		} catch (error) {
			return false;
		}
	}

	return Object.prototype.toString.call(item) === `[object URL]`;
}
