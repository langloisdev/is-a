import { isMap } from './map.js';
import { testItems } from '../tests/testItems.js';

delete testItems.testMap;

describe(`is map function`, () => {
	test(`exists`, () => {
		expect(isMap).toBeDefined();
	});

	test(`is a Function`, () => {
		expect(isMap).toBeInstanceOf(Function);
	});

	test.each([new Map()])(`returns true if the value passed is a map`, map => {
		expect(isMap(map)).toStrictEqual(true);
	});

	test.each(Object.entries(testItems))(`returns false if the value passed is %s`, (key, value) => {
		expect(isMap(value)).toStrictEqual(false);
	});
});
