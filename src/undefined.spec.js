import { isUndefined } from './undefined.js';
import { testItems } from '../tests/testItems.js';

delete testItems.testUndefined;

describe(`is undefined function`, () => {
	test(`exists`, () => {
		expect(isUndefined).toBeDefined();
	});

	test(`is a Function`, () => {
		expect(isUndefined).toBeInstanceOf(Function);
	});

	test(`returns true if the value passed is undefined`, () => {
		expect(isUndefined(undefined)).toStrictEqual(true);
	});

	test.each(Object.entries(testItems))(`returns false if the value passed is %s`, (key, value) => {
		expect(isUndefined(value)).toStrictEqual(false);
	});
});
