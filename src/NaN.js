/**
 * Evalutes if the item passed is NaN (not a number). Is a wrapper for the [Number.isNaN]{@link https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number/isNaN} function.
 *
 * @name isNaN
 * @function
 * @param {*} item - The data who's type is being evaluted.
 *
 * @example
 *
 * isNaN(NaN); // true
 */
export function isNaN (item) {
	return Number.isNaN(item);
}
