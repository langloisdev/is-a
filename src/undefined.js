/**
 * Evalutes if the item passed is undefined.
 *
 * @name isUndefined
 * @function
 * @param {*} item - The data who's type is being evaluted.
 *
 * @example
 *
 * isUndefined(undefined); // true
 */
export function isUndefined (item) {
	return item === void 0;
}
