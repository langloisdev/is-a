import { isNumber } from './number.js';
import { testItems } from '../tests/testItems.js';

delete testItems.testNumber;
delete testItems.testNegativeNumber;
delete testItems.testDecimalNumber;
delete testItems.testNegativeDecimalNumber;
delete testItems.testZero;
delete testItems.testNegativeZero;
delete testItems.testInfinity;

describe(`is number function`, () => {
	test(`exists`, () => {
		expect(isNumber).toBeDefined();
	});

	test(`is a Function`, () => {
		expect(isNumber).toBeInstanceOf(Function);
	});

	test.each([
		1,
		-1,
		1.2,
		-1.2,
		0,
		-0,
		0.0,
		-0.0,
		Infinity
	])(`returns true if the value passed is a number "%s"`, number => {
		expect(isNumber(number)).toStrictEqual(true);
	});

	test(`returns false if the value passed is NaN`, () => {
		expect(isNumber(NaN)).toStrictEqual(false);
	});

	test.each(Object.entries(testItems))(`returns false if the value passed is %s`, (key, value) => {
		expect(isNumber(value)).toStrictEqual(false);
	});
});
