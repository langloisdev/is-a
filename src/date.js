/**
 * Evalutes if the item passed is a date.
 *
 * @name isDate
 * @function
 * @param {*} item - The data who's type is being evaluted.
 *
 * @example
 *
 * isDate(new Date()); // true
 */
export function isDate (item) {
	return Object.prototype.toString.call(item) === `[object Date]`;
}
