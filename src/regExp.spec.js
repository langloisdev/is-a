import { isRegExp } from './regExp.js';
import { testItems } from '../tests/testItems.js';

delete testItems.testRegExp;

describe(`is regExp function`, () => {
	test(`exists`, () => {
		expect(isRegExp).toBeDefined();
	});

	test(`is a Function`, () => {
		expect(isRegExp).toBeInstanceOf(Function);
	});

	test(`returns true if the value passed is a regExp`, () => {
		expect(isRegExp(/\d/)).toStrictEqual(true);
	});

	test.each(Object.entries(testItems))(`returns false if the value passed is %s`, (key, value) => {
		expect(isRegExp(value)).toStrictEqual(false);
	});
});
