import { isUri } from './uri.js';
import { testItems } from '../tests/testItems.js';

describe(`is URI function`, () => {
	test(`exists`, () => {
		expect(isUri).toBeDefined();
	});

	test(`is a Function`, () => {
		expect(isUri).toBeInstanceOf(Function);
	});

	test.each([
		`https://langlois.dev`,
		`https://langlois.dev?q=abc#top`,
		`http://langlois.dev`,
		`http://localhost:123`,
		`http://127.0.0.1:123`,
		`https:langlois.dev`,
		`ldap://[2001:db8::7]/c=GB`,
		`news:comp.infosystems.www.servers.unix`,
		`mailto:francis@langlois.dev`,
		`tel:+1-123-123-1234`,
		`urn:franky:langlois`
	])(`returns true if the value passed is a valid URI "%s"`, uri => {
		expect(isUri(uri)).toStrictEqual(true);
	});

	test.each(Object.entries(testItems))(`returns false if the value passed is %s`, (key, value) => {
		expect(isUri(value)).toStrictEqual(false);
	});
});
