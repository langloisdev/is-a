import { isSymbol } from './symbol.js';
import { testItems } from '../tests/testItems.js';

delete testItems.testTrue;
delete testItems.testFalse;
delete testItems.testSymbol;

describe(`is symbol function`, () => {
	test(`exists`, () => {
		expect(isSymbol).toBeDefined();
	});

	test(`is a Function`, () => {
		expect(isSymbol).toBeInstanceOf(Function);
	});

	test(`returns true if the value passed is a symbol`, () => {
		expect(isSymbol(Symbol(`fakeSymbol`))).toStrictEqual(true);
	});

	test.each(Object.entries(testItems))(`returns false if the value passed is %s`, (key, value) => {
		expect(isSymbol(value)).toStrictEqual(false);
	});
});
