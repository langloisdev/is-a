import { isNull } from './null.js';
import { testItems } from '../tests/testItems.js';

delete testItems.testNull;

describe(`is null function`, () => {
	test(`exists`, () => {
		expect(isNull).toBeDefined();
	});

	test(`is a Function`, () => {
		expect(isNull).toBeInstanceOf(Function);
	});

	test(`returns true if the value passed is null`, () => {
		expect(isNull(null)).toStrictEqual(true);
	});

	test.each(Object.entries(testItems))(`returns false if the value passed is %s`, (key, value) => {
		expect(isNull(value)).toStrictEqual(false);
	});
});
