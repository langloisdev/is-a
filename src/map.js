/**
 * Evalutes if the item passed is a map.
 *
 * @name isMap
 * @function
 * @param {*} item - The data who's type is being evaluted.
 *
 * @example
 *
 * isMap(new Map()); // true
 */
export function isMap (item) {
	return Object.prototype.toString.call(item) === `[object Map]`;
}
