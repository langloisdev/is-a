/**
 * Evalutes if the item passed is a string.
 *
 * @name isString
 * @function
 * @param {*} item - The data who's type is being evaluted.
 *
 * @example
 *
 * isString(`foo`); // true
 */
export function isString (item) {
	return toString.call(item) === `[object String]`;
}
