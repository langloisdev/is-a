import { isObject } from './object.js';
import { testItems } from '../tests/testItems.js';

delete testItems.testObject;

describe(`is object function`, () => {
	test(`exists`, () => {
		expect(isObject).toBeDefined();
	});

	test(`is a Function`, () => {
		expect(isObject).toBeInstanceOf(Function);
	});

	test(`returns true if the value passed is an object`, () => {
		expect(isObject({})).toStrictEqual(true);
	});

	test.each(Object.entries(testItems))(`returns false if the value passed is %s`, (key, value) => {
		expect(isObject(value)).toStrictEqual(false);
	});
});
