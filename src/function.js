/**
 * Evalutes if the item passed is a function.
 *
 * @name isFunction
 * @function
 * @param {*} item - The data who's type is being evaluted.
 *
 * @example
 *
 * isFunction(() => undefined); // true
 * isFunction(function foo () {}); // true
 */
export function isFunction (item) {
	return typeof (item) === `function`;
}
