import { isString } from './string.js';

/**
 * Evalutes if the item passed is a UUID version 1 through 5.
 *
 * @name isUuid
 * @function
 * @param {*} item - The data who's type is being evaluted.
 *
 * @example
 *
 * isUuid(`a38bb2a7-470f-4063-baa8-1109d2da4bad`); // true
 */
export function isUuid (item) {
	if (!isString(item)) return false;

	const uuidRegex = new RegExp(`^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$`);

	return uuidRegex.test(item);
}
