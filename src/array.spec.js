import { isArray } from './array.js';
import { testItems } from '../tests/testItems.js';

delete testItems.testArray;

describe(`is array function`, () => {
	test(`exists`, () => {
		expect(isArray).toBeDefined();
	});

	test(`is a Function`, () => {
		expect(isArray).toBeInstanceOf(Function);
	});

	test.each(Object.entries({
		a: [],
		b: new Array(),
		c: [true, false, null, undefined]
	}))(`returns true if the value passed is an array`, (key, array) => {
		expect(isArray(array)).toStrictEqual(true);
	});

	test.each(Object.entries(testItems))(`returns false if the value passed is %s`, (key, value) => {
		expect(isArray(value)).toStrictEqual(false);
	});
});
