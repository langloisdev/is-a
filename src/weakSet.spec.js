import { isWeakSet } from './weakSet.js';
import { testItems } from '../tests/testItems.js';

delete testItems.testWeakSet;

describe(`is weak set function`, () => {
	test(`exists`, () => {
		expect(isWeakSet).toBeDefined();
	});

	test(`is a Function`, () => {
		expect(isWeakSet).toBeInstanceOf(Function);
	});

	test(`returns true if the value passed is a weak set`, () => {
		expect(isWeakSet(new WeakSet())).toStrictEqual(true);
	});

	test.each(Object.entries(testItems))(`returns false if the value passed is %s`, (key, value) => {
		expect(isWeakSet(value)).toStrictEqual(false);
	});
});
