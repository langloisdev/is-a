import { isError } from './error.js';
import { testItems } from '../tests/testItems.js';

delete testItems.testError;

describe(`is error function`, () => {
	test(`exists`, () => {
		expect(isError).toBeDefined();
	});

	test(`is a Function`, () => {
		expect(isError).toBeInstanceOf(Function);
	});

	test.each([
		new Error(),
		new TypeError(),
		new EvalError(),
		new RangeError(),
		new ReferenceError(),
		new SyntaxError(),
		new TypeError(),
		new URIError()
	])(`returns true if the value passed is an error`, error => {
		expect(isError(error)).toStrictEqual(true);
	});

	test.each(Object.entries(testItems))(`returns false if the value passed is %s`, (key, value) => {
		expect(isError(value)).toStrictEqual(false);
	});
});
