/**
 * Evalutes if the item passed is null.
 *
 * @name isNull
 * @function
 * @param {*} item - The data who's type is being evaluted.
 *
 * @example
 *
 * isNull(null); // true
 */
export function isNull (item) {
	return item === null;
}
