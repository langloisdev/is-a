import { isWeakMap } from './weakMap.js';
import { testItems } from '../tests/testItems.js';

delete testItems.testWeakMap;

describe(`is weak map function`, () => {
	test(`exists`, () => {
		expect(isWeakMap).toBeDefined();
	});

	test(`is a Function`, () => {
		expect(isWeakMap).toBeInstanceOf(Function);
	});

	test(`returns true if the value passed is a weak map`, () => {
		expect(isWeakMap(new WeakMap())).toStrictEqual(true);
	});

	test.each(Object.entries(testItems))(`returns false if the value passed is %s`, (key, value) => {
		expect(isWeakMap(value)).toStrictEqual(false);
	});
});
