/**
 * Evalutes if the item passed is a regExp.
 *
 * @name isRegExp
 * @function
 * @param {*} item - The data who's type is being evaluted.
 *
 * @example
 *
 * isRegExp(/\d/); // true
 * isRegExp(new RegExp()); // true
 */
export function isRegExp (item) {
	return Object.prototype.toString.call(item) === `[object RegExp]`;
}
