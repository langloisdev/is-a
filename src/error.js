/**
 * Evalutes if the item passed is an error.
 *
 * @name isError
 * @function
 * @param {*} item - The data who's type is being evaluted.
 *
 * @example
 *
 * isError(new Error()); // true
 */
export function isError (item) {
	return Object.prototype.toString.call(item) === `[object Error]`;
}
