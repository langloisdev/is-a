import { isEmail } from './email.js';
import { testItems, testStringEmpty, testStringNumber, testStringText} from '../tests/testItems.js';

describe(`is email function`, () => {
	let testStrings;

	beforeEach(() => {
		testStrings = [testStringEmpty, testStringNumber, testStringText];
	});

	test(`exists`, () => {
		expect(isEmail).toBeDefined();
	});

	test(`is a Function`, () => {
		expect(isEmail).toBeInstanceOf(Function);
	});

	test.each([
		`franky@langlois.dev`,
		`franky123@langlois.dev`,
		`franky+abc@langlois.dev`,
		`franky.abc@langlois.dev`,
		`franky-abc@langlois.dev`,
		`franky@langlois`
	])(`returns true if the value passed is an email "%s"`, email => {
		expect(isEmail(email)).toStrictEqual(true);
	});

	test.each(Object.entries(testItems))(`returns false if the value passed is %s`, (key, value) => {
		expect(isEmail(value)).toStrictEqual(false);
	});
});
