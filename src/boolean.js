/**
 * Evalutes if the item passed is a boolean.
 *
 * @name isBoolean
 * @function
 * @param {*} item - The data who's type is being evaluted.
 *
 * @example
 *
 * isBoolean(true); // true
 * isBoolean(false); // true
 */
export function isBoolean (item) {
	return Object.prototype.toString.call(item) === `[object Boolean]`;
}
