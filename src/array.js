/**
 * Evalutes if the item passed is an array.
 *
 * @name isArray
 * @function
 * @param {*} item - The data who's type is being evaluted.
 *
 * @example
 *
 * isArray([]); // true
 */
export function isArray (item) {
	return Array.isArray(item);
}
