/**
 * Evalutes if the item passed is a weak set.
 *
 * @name isWeakSet
 * @function
 * @param {*} item - The data who's type is being evaluted.
 *
 * @example
 *
 * isWeakSet(new WeakSet()); // true
 */
export function isWeakSet (item) {
	return Object.prototype.toString.call(item) === `[object WeakSet]`;
}
