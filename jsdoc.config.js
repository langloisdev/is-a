module.exports = {
	recurseDepth: 10,
	source: {
		include: [
			`./index.js`,
			`./src`
		],
		exclude: [
			`./node_modules/`
		],
		includePattern: `.js$`,
		excludePattern: `(^|\\/|\\\\)_|.+.spec.`
	},
	sourceType: `module`,
	tags: {
		allowUnknownTags: true,
		dictionaries: [
			`closure`,
			`jsdoc`
		]
	},
	opts: {
		recurse: true,
		template: `./node_modules/clean-jsdoc-theme`,
		verbose: true
	},
	templates: {
		cleverLinks: false,
		monospaceLinks: false,
		search: true,
		'clean-jsdoc-theme': {
			name: `Is A Library`,
			title: `Is A Library`
		}
	}
};
