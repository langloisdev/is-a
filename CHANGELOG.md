# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

<!-- ## Unreleased -->
<!-- ### Added -->
<!-- ### Bug Fixes -->
<!-- ### Changed -->

## [4.3.0] 2022-05-04
### Added
- `.uuid` to detect if the param passed is a UUID

## [4.2.0] 2020-12-29
### Added
- webpack to bundle library

### Changed
- email regex validation now accepts unicodes and letters from other languages

## [4.0.0 - 4.1.2] 2020-12-27
### Added
- `.email` API endpoint to detect if the param passed is a valid email
- `.uri` API endpoint to detect if the param passed is a valid URI or a string that resolves to a valid URI

### Changed
- import using named export
- each rule refactored to its own file

## [3.0.1 - 3.0.2] 2019-08-19
### Added
- test coverage
- made public

## [3.0.0] 2019-08-19
### Added
- `.arguments` API endpoint to detect if the param passed is a function's argument list
- `.error` API endpoint to detect if the param passed is an error
- `.map` API endpoint to detect if the param passed is a Map instance
- `.NaN` API endpoint to detect if the param passed is `NaN`
- `.null` API endpoint to detect if the param passed is `null`
- `.set` API endpoint to detect if the param passed is a Set instance
- `.symbol` API endpoint to detect if the param passed is a Symbol instance
- `.undefined` API endpoint to detect if the param passed is `undefined`
- `.weakMap` API endpoint to detect if the param passed is a WeakMap instance
- `.weakSet` API endpoint to detect if the param passed is a WeakSet instance
### Bug Fixes
- `.numerical` no longer returns `true` when passed an empty string
### Changed
- `.number` returns false when passed `NaN`
- `.object` won't return true for the following data types:
  - dates
  - errors
  - maps
  - regExps
  - sets
  - weakMaps
  - weakSets

## [2.1.0] 2019-08-15
- isA.string(item) - returns true if a string is passed
- isA.numerical(item) - returns true if a number or a string that represents a valid number is passed, except `NaN`

## [2.0.0 - 2.0.3] 2019-08-15
- renamed module from @langlois/is-a-utils-js to @langlois/is-a
- compile and minify code

## [1.0.2]
### Added
- fixed installation documentation

## [1.0.1]
### Bug Fixes
- ci no longer needlessly tries to compile

## [1.0.0]
### Added
- isA API
- tests
